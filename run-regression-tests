#!/bin/bash

# Default values
NUM_RANKS=1           # Default to 1 rank if no rank is provided
CREATE_REF_SOL=false  # Default is not to create reference solution

# Parse arguments
for arg in "$@"; do
  if [ "$arg" == "--create-ref-sol" ]; then
    CREATE_REF_SOL=true
  elif [[ "$arg" =~ ^[0-9]+$ ]]; then
    NUM_RANKS=$arg  # If the argument is a number, treat it as the number of ranks
  else
    echo "Unknown argument: $arg"
    exit 1
  fi
done

# Check if number of ranks is provided as an argument
if [ -z "$1" ]; then
    NUM_RANKS=1
else
    # Assign the number of ranks to a variable
    NUM_RANKS=$1
fi

# Loop over all .in files in the current directory
for input_file in *.in; do

  # Extract the basename (filename without the extension)
  base_name=$(basename "$input_file" .in)

  # Define the output file
  output_file="${base_name}.p${NUM_RANKS}.out"
  
  echo "Processing $input_file with $NUM_RANKS ranks..."
  mpirun -np ${NUM_RANKS} opalx "$input_file" --info 5 >  "$output_file" 2>&1

  if [ "$CREATE_REF_SOL" == true ]; then
      read -p "Are you sure you want to create the reference solution? (Y/N): " CONFIRM

  # Check if the user typed 'Y'
  if [[ "$CONFIRM" == "Y" || "$CONFIRM" == "y" ]]; then
      echo "Creating reference solution..."
      mv "$output_file" reference-solution/"$output_file"
  else
      echo "Reference solution creation skipped."
  fi
 fi

 diff <(grep -v -f reference-solution/veto.txt $output_file) <(grep -v -f reference-solution/veto.txt reference-solution/$output_file)

 if [ $? -eq 0 ]; then
      echo "The files $output_file match after ignoring the specified keywords in reference-solution/veto.txt."
 else
      echo "The files differ."
 fi
done
