# regresion-test-x

## Purpose

Create and run OPALX regression tests

## Add a new regression test and generate reference solution


## Check an OPALX run against reference solutions

- [ ] [Set up project integrations](https://gitlab.psi.ch/OPAL/regresion-test-x/-/settings/integrations)

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thanks to [makeareadme.com](https://www.makeareadme.com/) for this template.

