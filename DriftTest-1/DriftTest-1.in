/*      DriftTest-1.in
	OPAL-X
	Test simple Gaussian distribution with space charge in a 2 m drift
*/

OPTION, PSDUMPFREQ      = 1;     // 6d data written every 10th time step (h5).
OPTION, STATDUMPFREQ    = 1;     // Beam Stats written every time step (stat).
OPTION, BOUNDPDESTROYFQ = 10;    // Delete lost particles, if any out of 10 \sigma
OPTION, AUTOPHASE       = 4;     // Autophase is on, and phase of max energy
                                 // gain will be found automatically for cavities.
OPTION, VERSION=10900;

Title, string="Test simple Gaussian distribution with space charge";

Value,{OPALVERSION};

// ----------------------------------------------------------------------------
// Global Parameters

REAL rf_freq             = 1.3e3;    // RF frequency. (MHz)
REAL n_particles         = 1E4;      // Number of particles in simulation.
REAL beam_bunch_charge   = 1e-9;     // Charge of bunch. (C)

// ----------------------------------------------------------------------------
// Initial Momentum Calculation

REAL Edes    = 1.4e-9; //initial energy in GeV
REAL gamma   = (Edes+EMASS)/EMASS; 
REAL beta    = sqrt(1-(1/gamma^2));
REAL P0      = gamma*beta*EMASS;    //inital z momentum

//Printing initial energy and momentum to terminal output.
value, {Edes, P0, OPALVERSION};

DR1: DRIFT, L = 0.25, ELEMEDGE = 0.0;

DrLine: Line = (DR1);

Dist1: DISTRIBUTION, TYPE=GAUSS,
SIGMAX=0.00075,
SIGMAY=0.00075,
SIGMAZ=0.0005,
SIGMAPX=0.0,
SIGMAPY=0.0,
SIGMAPZ=0.0;

FS1: Fieldsolver, Type=FFT,
     NX = 16, NY = 16, NZ = 16,
     PARFFTX = TRUE,
     PARFFTY = TRUE,
     PARFFTZ = TRUE,
     BCFFTX = OPEN,
     BCFFTY = OPEN,
     BCFFTZ = OPEN,
     BBOXINCR = 1, GREENSF = STANDARD;


BEAM1:  BEAM, PARTICLE = ELECTRON, pc = P0, NPART = n_particles,
        BFREQ = rf_freq, BCURRENT = beam_bunch_charge * rf_freq, CHARGE = -1;

TRACK, LINE = DrLine, BEAM = BEAM1, MAXSTEPS = 1, DT = {1e-10}, ZSTOP=0.25; 
 RUN, METHOD = "PARALLEL", BEAM = BEAM1, FIELDSOLVER = FS1, DISTRIBUTION = Dist1;
ENDTRACK;
QUIT;
